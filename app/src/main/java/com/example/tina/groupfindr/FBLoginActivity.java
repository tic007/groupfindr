package com.example.tina.groupfindr;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import io.fabric.sdk.android.Fabric;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class FBLoginActivity extends AppCompatActivity {

    private LoginButton button;
    private CallbackManager callbackManager;
    private RequestQueue jsonQueue;

    public boolean isLoggedIn() {
        boolean loggedIn=false;
        if(AccessToken.getCurrentAccessToken()!=null)
            loggedIn=true;
        return loggedIn;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_fblogin);

        try {
            PackageInfo info =
                    getPackageManager().getPackageInfo("com.example.tina.groupfindr",
                                                       PackageManager.GET_SIGNATURES);

            for(Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            //name not found
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            //no such algorithm exception
            e.printStackTrace();
        }

        jsonQueue = VolleyQueue.getInstance(this.getApplicationContext()).ourInstance;
        callbackManager = CallbackManager.Factory.create();
        button = (LoginButton) findViewById(R.id.login_button);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(isLoggedIn()) {
            final String userID = AccessToken.getCurrentAccessToken().getUserId();
            final String tokenID = AccessToken.getCurrentAccessToken().getToken();
            Log.e("CSE 190 FB Login Demo: ", "onSuccess : ID = " +
                    userID + "\nToken = " + tokenID);

            final JSONObject objectToPost = new JSONObject();
            try {
                objectToPost.put("Facebook_Token", userID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
            Analytics application = (Analytics) getApplication();
            final Tracker mTracker = application.getDefaultTracker();

            String testURL;

            testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/users";

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, testURL,
                    objectToPost, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    // Parse response from POST Call

                    try {
                        /*
                        User_Model curUser = new User_Model(response.getString("Description"), response.getString("Name"), response.getString("Facebook_Token"));

                        System.out.println("Name: " + curUser.Name);
                        System.out.println("Description: " + curUser.Description);
                        System.out.println("Facebook_Token: " + curUser.Facebook_Token);
                        */
                        JSONObject curUser = response.getJSONObject("user");
                        System.out.println(response);
                        //User_Model curUser = new User_Model("", "", userID);
                        if ((curUser.has("Description") && (curUser.isNull("Description") || curUser.getString("Description").isEmpty())) || (curUser.has("Name") && (curUser.isNull("Name") || curUser.getString("Name").isEmpty()))) {
                            Intent intent = new Intent(getApplicationContext(),UserInfoActivity.class);
                            intent.putExtra("UserID", userID);
                            intent.putExtra("TokenID", tokenID);
                            intent.putExtra("newUser", true);
                            startActivity(intent);
                            finish();

                            // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                            Log.i("FBLoginActivity", "Setting screen name: New User");
                            mTracker.setScreenName("New User");
                            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                            return;
                        }

                        Intent intent = new Intent(getApplicationContext(),
                                JoinSessionActivity.class);
                        intent.putExtra("UserID", AccessToken.getCurrentAccessToken().getUserId());
                        intent.putExtra("TokenID", AccessToken.getCurrentAccessToken().getToken());
                        intent.putExtra("UserName", curUser.getString("Name"));
                        intent.putExtra("UserDescription", curUser.getString("Description"));
                        startActivity(intent);
                        finish();

                        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                        Log.i("FBLoginActivity", "Setting screen name: Returning User");
                        mTracker.setScreenName("Returning User");
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                        return;
                    }catch(JSONException e){
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "An error occurred during login. Please try again.", Toast.LENGTH_LONG).show();
                    }
                    /*
                    Intent intent = new Intent(getApplicationContext(),
                            ClassCreationActivity.class);
                    intent.putExtra("UserID", userID);
                    intent.putExtra("TokenID", tokenID);
                    startActivity(intent);
                    finish();
                    */
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // Handle Errors
                    System.out.println(error.getMessage() + ": " + error.getStackTrace());
                    Toast.makeText(getApplicationContext(), "An error occurred during login. Please try again.", Toast.LENGTH_LONG).show();
                }
            });

            jsonQueue.add(postRequest);
            return;
        }


        button.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    /******************************************************************
                     * Method called when the user successfully logs in. You can
                     * redirect the user to another activity in this call (like I coded
                     * below).
                     * @param loginResult - Contains granted permissions, AccessTokens
                     *                      AccessTokens = appID, userID, tokenID, etc.
                     *                      You can use the information here to add to
                     *                      your database
                     *****************************************************************/
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        final String userID = loginResult.getAccessToken().getUserId();
                        final String tokenID = loginResult.getAccessToken().getToken();
                        Log.e("CSE 190 FB Login Demo: ", "onSuccess : ID = " +
                                userID + "\nToken = " + tokenID);

                        final JSONObject objectToPost = new JSONObject();
                        try {
                            objectToPost.put("Facebook_Token", userID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String testURL;

                        testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/users";

                        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, testURL,
                                objectToPost, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Parse response from POST Call
                                /*// Code to print out enrolled classes
                                JSONArray classes_enrolled;
                                try{
                                    classes_enrolled = response.getJSONArray("Classes_Enrolled");
                                    for(int i = 0; i < classes_enrolled.length(); i++) {
                                        System.out.println("");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                //*/
                                try {
                                    /*
                                    User_Model curUser = new User_Model(response.getString("Description"), response.getString("Name"), response.getString("Facebook_Token"));

                                    System.out.println("Name: " + curUser.Name);
                                    System.out.println("Description: " + curUser.Description);
                                    System.out.println("Facebook_Token: " + curUser.Facebook_Token);

                                    Intent intent = new Intent(getApplicationContext(),
                                            ClassCreationActivity.class);
                                    intent.putExtra("UserID", userID);
                                    intent.putExtra("TokenID", tokenID);
                                    startActivity(intent);
                                    finish();
                                    */
                                    JSONObject curUser = response.getJSONObject("user");
                                    System.out.println(response);
                                    //User_Model curUser = new User_Model("", "", userID);
                                    if ((curUser.has("Description") && (curUser.isNull("Description") || curUser.getString("Description").isEmpty())) || (curUser.has("Name") && (curUser.isNull("Name") || curUser.getString("Name").isEmpty()))) {
                                        Intent intent = new Intent(getApplicationContext(),UserInfoActivity.class);
                                        intent.putExtra("UserID", userID);
                                        intent.putExtra("TokenID", tokenID);
                                        intent.putExtra("newUser", true);
                                        startActivity(intent);
                                        finish();
                                        return;
                                    }

                                    Intent intent = new Intent(getApplicationContext(),
                                            JoinSessionActivity.class);
                                    intent.putExtra("UserID", userID);
                                    intent.putExtra("TokenID", tokenID);
                                    intent.putExtra("UserName", curUser.getString("Name"));
                                    intent.putExtra("UserDescription", curUser.getString("Description"));
                                    startActivity(intent);
                                    finish();
                                    return;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "An error occurred during login. Please try again.", Toast.LENGTH_LONG).show();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Handle Errors
                                System.out.println(error.getMessage() + ": " + error.getStackTrace());
                                Toast.makeText(getApplicationContext(), "An error occurred during login. Please try again.", Toast.LENGTH_LONG).show();
                            }
                        });

                        jsonQueue.add(postRequest);

                        /**************************************************************
                         * Creates a new Intent to begin and pass in the userID and
                         * tokenID to the next Activity. Calling finish() prevents the
                         * user from returning to this Activity.
                         *
                         * Do note that you do not need to put in the Extras for the
                         * User ID and Token ID as they are available through:
                         * AccessTokens.getCurrentAccessToken().getUserID() or
                         * AccessTokens.getCurrentAccessToken().getTokenID()
                         *************************************************************/

                    }

                    /******************************************************************
                     * Method called when the user cancels the login attempt
                     *****************************************************************/
                    @Override
                    public void onCancel() {
                        Log.e("CSE 190 FB Login Demo: ", "onCancel");
                        Toast.makeText(getApplicationContext(), "Facebook login cancelled. Please login to continue.", Toast.LENGTH_LONG).show();
                    }

                    /******************************************************************
                     * Method called when an error occurs with login
                     * @param error - A FacebookException/ Error
                     *****************************************************************/
                    @Override
                    public void onError(FacebookException error) {
                        Log.e("CSE 190 FB Login Demo: ", "onError");
                        Toast.makeText(getApplicationContext(), "An error occurred during login. Please try again.", Toast.LENGTH_LONG).show();
                    }
                });

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent
            data) {
        callbackManager.onActivityResult(requestCode, resultCode,
                data);
    }


}
