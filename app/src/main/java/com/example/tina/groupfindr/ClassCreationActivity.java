package com.example.tina.groupfindr;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

public class ClassCreationActivity extends AppCompatActivity {

    private Button mCreateClassButton;
    private EditText mClassName;
    private EditText mClassDescription;
    private RequestQueue jsonQueue;
    private Intent sentIntent; //this intent is sent to this activity

    // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
    private Analytics application; //= (Analytics) getApplication();
    private Tracker mTracker; //= application.getDefaultTracker();

    //boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        sentIntent.setClass(getApplicationContext(), JoinSessionActivity.class);
        startActivity(sentIntent);
        finish();

        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
        Log.i("ClassCreationActivity", "Setting screen name: Backing to Join");
        mTracker.setScreenName("Backing to Join");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_creation);

        application = (Analytics) getApplication();
        mTracker = application.getDefaultTracker();

        mCreateClassButton = (Button) findViewById(R.id.classCreation_createClassButton);
        mClassName = (EditText) findViewById(R.id.userInfo_UserName);
        mClassDescription = (EditText) findViewById(R.id.classCreation_classDescriptionField);
        jsonQueue = VolleyQueue.getInstance(this.getApplicationContext()).ourInstance;
        sentIntent = getIntent();

        mCreateClassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ClassCreationActivity.this, mClassName.getText() + " " + mClassDescription.getText(), Toast.LENGTH_SHORT).show();

                if(mClassName.getText().toString().isEmpty() || mClassDescription.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Please fill in both fields to continue.", Toast.LENGTH_LONG).show();
                    return;
                }

                final JSONObject objectToPost = new JSONObject();
                try {
                    objectToPost.put("name", mClassName.getText());
                    objectToPost.put("description", mClassDescription.getText());
                    objectToPost.put("Creator_ID", getIntent().getStringExtra("UserID"));
                    objectToPost.put("Facebook_Token", getIntent().getStringExtra("UserID"));
                    //objectToPost.put("Creator_ID", sentIntent.getStringExtra("User_ID"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String testURL;
                //try {
                testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/class";

                JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, testURL,
                        objectToPost, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e(TAG, "jsonObject = " + response.toString());
                        /******************************************************
                         * try {
                         *   JSON Manipulation occurs here. You need to know
                         *   how the JSON you are parsing is ordered to be able
                         *   to pull the String/ values from it.
                         * } catch (JSONException e) {
                         *   e.printStackTrace();
                         * }
                         *****************************************************/



                        try {
                            System.out.println(response);
                            if(!response.getString("success").equals("true")) {
                                System.err.println("NO RESPONSE");
                                return;
                            }
                            JSONObject newClass = response.getJSONObject("classes");
                            String desc = newClass.getString("Description");
                            String name = newClass.getString("Name");
                            //String desc = "desc";
                            //String name = "name";
                            String classCode = newClass.getString("Hashcode");


                            System.out.println("name = " + name + " | Description = " + desc + " | classCode = " + classCode);

                            sentIntent.setClass(getApplicationContext(), ClassCreationConfirmationActivity.class);
                            sentIntent.putExtra("ClassName", newClass.getString("Name"));
                            sentIntent.putExtra("ClassDescription", newClass.getString("Description"));
                            sentIntent.putExtra("ClassCode", newClass.getString("Hashcode"));
                            startActivity(sentIntent);
                            finish();

                            // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                            Log.i("ClassCreationActivity", "Setting screen name: Creating Class");
                            mTracker.setScreenName("Creating Class");
                            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                            return;

                        } catch (JSONException e) {
                            //Log.e(TAG, "error");
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "An error occurred while creating class. Please try again.", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle Errors
                        System.out.println(error.getMessage() + ": " + error.getStackTrace());
                        Toast.makeText(getApplicationContext(), "An error occurred while creating class. Please try again.", Toast.LENGTH_LONG).show();
                    }
                });

                jsonQueue.add(postRequest);
            }
        });


        /*
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.GET, URL, new
                Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject obj)
                    {
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError v)
                    {
                        // Insert Error Handling
                    }
                }
        );
        */
    }



}
