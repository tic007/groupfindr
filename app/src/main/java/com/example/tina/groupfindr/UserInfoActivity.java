package com.example.tina.groupfindr;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

public class UserInfoActivity extends AppCompatActivity {

    private Button mSaveInfoButton;
    private EditText mUserName;
    private EditText mUserDescription;
    private RequestQueue jsonQueue;
    private Intent sentIntent; //this intent is sent to this activity
    private Bundle intentBundle;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if(intentBundle.getBoolean("newUser"))
        {
            if (doubleBackToExitPressedOnce) {
                android.os.Process.killProcess(android.os.Process.myPid());
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
        else
        {
            sentIntent.setClass(getApplicationContext(), JoinSessionActivity.class);
            startActivity(sentIntent);
            finish();
            return;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_info);

        mSaveInfoButton = (Button) findViewById(R.id.userInfo_SaveInfoButton);
        mUserName = (EditText) findViewById(R.id.userInfo_UserName);
        mUserDescription = (EditText) findViewById(R.id.userInfo_UserDescription);
        jsonQueue = VolleyQueue.getInstance(this.getApplicationContext()).ourInstance;
        sentIntent = getIntent();
        intentBundle = sentIntent.getExtras();
        System.out.println("UserID: " + intentBundle.getString("UserID"));

        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
        Analytics application = (Analytics) getApplication();
        final Tracker mTracker = application.getDefaultTracker();

        if(intentBundle.getString("UserName") != null && !intentBundle.getString("UserName").equals(""))
        {
            mUserName.setText(intentBundle.getString("UserName"));
        }
        if(intentBundle.getString("UserDescription") != null && !intentBundle.getString("UserDescription").equals(""))
        {
            mUserDescription.setText(intentBundle.getString("UserDescription"));
        }


        mSaveInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(UserInfoActivity.this, mUserName.getText() + " " + mUserDescription.getText(), Toast.LENGTH_SHORT).show();
                if(mUserName.getText().toString().isEmpty() || mUserDescription.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Please fill in both fields to continue.", Toast.LENGTH_LONG).show();
                    return;
                }

                final JSONObject objectToPost = new JSONObject();
                System.out.println("Name: " + mUserName.getText());
                System.out.println("Description: " + mUserDescription.getText());
                System.out.println("UserID: " + intentBundle.getString("UserID"));
                System.out.println("");
                try {
                    objectToPost.put("Name", mUserName.getText());
                    objectToPost.put("Description", mUserDescription.getText());
                    objectToPost.put("Facebook_Token", intentBundle.getString("UserID"));
                    //objectToPost.put("Creator_ID", sentIntent.getStringExtra("User_ID"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String testURL;
                //try {
                testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/updateUser";

                JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, testURL,
                        objectToPost, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e(TAG, "jsonObject = " + response.toString());
                        /******************************************************
                         * try {
                         *   JSON Manipulation occurs here. You need to know
                         *   how the JSON you are parsing is ordered to be able
                         *   to pull the String/ values from it.
                         * } catch (JSONException e) {
                         *   e.printStackTrace();
                         * }
                         *****************************************************/



                        try {
                            if(!response.getString("success").equals("true")) {
                                System.err.println("Error encountered");
                                Toast.makeText(getApplicationContext(), "An error occurred while updating information. Please try again.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            else
                            {
                                String userName = mUserName.getText().toString();
                                String userDescription = mUserDescription.getText().toString();
                                System.out.println(mUserName.getText());
                                System.out.println(mUserDescription.getText());
                                Intent intent = new Intent(getApplicationContext(), JoinSessionActivity.class);
                                intent.putExtra("UserID", intentBundle.getString("UserID"));
                                intent.putExtra("TokenID", intentBundle.getString("TokenID"));
                                intent.putExtra("UserName", userName);
                                intent.putExtra("UserDescription", userDescription);
                                startActivity(intent);
                                finish();

                                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                                Log.i("UserInfoActivity", "Setting screen name: Save");
                                mTracker.setScreenName("Save");
                                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                                return;
                            }
                            //JSONObject newClass = response.getJSONObject("classes");
                            //String desc = newClass.getString("Description");
                            //String name = newClass.getString("Name");
                            //String desc = "desc";
                            //String name = "name";
                            //String classCode = newClass.getString("Hashcode");


                            //System.out.println("name = " + name + " | Description = " + desc + " | classCode = " + classCode);



                        } catch (JSONException e) {
                            //Log.e(TAG, "error");
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "An error occurred while updating information. Please try again.", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle Errors
                        System.out.println(error.getMessage() + ": " + error.getStackTrace());
                        Toast.makeText(getApplicationContext(), "An error occurred while updating information. Please try again.", Toast.LENGTH_LONG).show();
                    }
                });

                jsonQueue.add(postRequest);
            }
        });


        /*
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.GET, URL, new
                Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject obj)
                    {
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError v)
                    {
                        // Insert Error Handling
                    }
                }
        );
        */
    }



}
