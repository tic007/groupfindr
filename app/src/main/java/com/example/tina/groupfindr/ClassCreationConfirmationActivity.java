package com.example.tina.groupfindr;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class ClassCreationConfirmationActivity extends AppCompatActivity {
    TextView mClassCode;
    TextView mClassName;
    Button mReturnButton;
    Intent sentIntent;
    Bundle intentBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_creation_confirmation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sentIntent = getIntent();
        intentBundle = sentIntent.getExtras();
        mClassCode = (TextView) findViewById(R.id.ClassCreationConfirmation_CodeText);
        mClassName = (TextView) findViewById(R.id.ClassCreationConfirmation_ClassName);
        mReturnButton = (Button) findViewById(R.id.ClassCreationConfirmation_ReturnButton);
        mClassCode.setText(intentBundle.getString("ClassCode"));
        mClassName.setText(intentBundle.getString("ClassName"));

        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
        Analytics application = (Analytics) getApplication();
        final Tracker mTracker = application.getDefaultTracker();

        mReturnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sentIntent.setClass(getApplicationContext(), JoinSessionActivity.class);
                startActivity(sentIntent);
                finish();

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("ConfirmationActivity", "Setting screen name: Class Confirmation");
                mTracker.setScreenName("Class Confirmation");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                return;
            }
        });
    }

}
