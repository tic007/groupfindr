package com.example.tina.groupfindr;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Tina on 11/17/15.
 */
public class VolleyQueue {
    public RequestQueue ourInstance;
    private static VolleyQueue volleyInstance;

    public static VolleyQueue getInstance(Context c) {
        if(volleyInstance == null) {
            volleyInstance = new VolleyQueue(c);
        }
        return volleyInstance;
    }

    private VolleyQueue(Context c) {
        if(this.ourInstance == null) {
            this.ourInstance = Volley.newRequestQueue(c.getApplicationContext());
        }
    }
}
