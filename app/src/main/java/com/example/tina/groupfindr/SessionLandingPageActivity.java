package com.example.tina.groupfindr;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SessionLandingPageActivity extends AppCompatActivity {


    private EditText GroupText;
    private EditText StudentText;
    private Button EnrollmentButton;
    private TextView ClassName;
    private TextView ClassCode;
    private Intent sentIntent;
    private Bundle intentBundle;
    private RequestQueue jsonQueue;

    private Boolean isEnrolled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_landing_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
        Analytics application = (Analytics) getApplication();
        final Tracker mTracker = application.getDefaultTracker();

        sentIntent = getIntent();
        intentBundle = sentIntent.getExtras();
        jsonQueue = VolleyQueue.getInstance(this.getApplicationContext()).ourInstance;

        isEnrolled = intentBundle.getBoolean("Enrolled");

        ClassName = (TextView) findViewById(R.id.SessionLanding_ClassName);
        ClassCode = (TextView) findViewById(R.id.SessionLanding_ClassCode);
        EnrollmentButton = (Button) findViewById(R.id.SessionLanding_EnrollmentButton);

        //System.out.println("Enrolled: " + intentBundle.getBoolean("Enrolled"));

        if(intentBundle.getString("ClassName") != null && !intentBundle.getString("ClassName").equals("")) {
            ClassName.setText(intentBundle.getString("ClassName"));
        }
        if(intentBundle.getString("ClassCode") != null && !intentBundle.getString("ClassCode").equals("")) {
            ClassName.setText(intentBundle.getString("ClassCode"));
        }
        if(isEnrolled) {
            EnrollmentButton.setText("Cancel enrollment to hide your profile from others");
        }
        else{
            EnrollmentButton.setText("Enroll in this class for others to see you");
        }

        /*
        GroupText = (EditText) findViewById(R.id.SessionLanding_ViewGroupsText);
        */
        StudentText = (EditText) findViewById(R.id.SessionLanding_ViewStudentsText);


        EnrollmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEnrolled) {
                    final JSONObject objectToPost = new JSONObject();
                    try {
                        objectToPost.put("classid", intentBundle.getString("ClassCode"));
                        objectToPost.put("Facebook_Token", intentBundle.getString("UserID"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String testURL;
                    //try {
                    testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/enrollment";

                    JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, testURL,
                            objectToPost, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //Log.e(TAG, "jsonObject = " + response.toString());
                            /******************************************************
                             * try {
                             *   JSON Manipulation occurs here. You need to know
                             *   how the JSON you are parsing is ordered to be able
                             *   to pull the String/ values from it.
                             * } catch (JSONException e) {
                             *   e.printStackTrace();
                             * }
                             *****************************************************/


                            try {
                                System.out.println(response);
                                if (!response.getString("success").equals("true")) {
                                    //System.err.println("NO RESPONSE");
                                    Toast.makeText(getApplicationContext(), "An error occurred while enrolling in class. Please try again.", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                JSONObject newEnrollment = response.getJSONObject("enrollment");
                                //String desc = "desc";
                                //String name = "name";
                                //String classCode = newClass.getString("Hashcode");


                                //System.out.println("name = " + name + " | Description = " + desc + " | classCode = " + classCode);
                                /*
                                sentIntent.setClass(getApplicationContext(), ClassCreationConfirmationActivity.class);
                                sentIntent.putExtra("ClassName", newClass.getString("Name"));
                                sentIntent.putExtra("ClassDescription", newClass.getString("Description"));
                                sentIntent.putExtra("ClassCode", newClass.getString("Hashcode"));
                                startActivity(sentIntent);
                                finish();
                                */
                                Intent nextIntent = new Intent(getApplicationContext(), StudentTindrActivity.class);
                                nextIntent.putExtra("UserID", intentBundle.getString("UserID"));
                                nextIntent.putExtra("TokenID", intentBundle.getString("TokenID"));
                                nextIntent.putExtra("UserName", intentBundle.getString("UserName"));
                                nextIntent.putExtra("UserDescription", intentBundle.getString("UserDescription"));
                                nextIntent.putExtra("ClassName", intentBundle.getString("ClassName"));
                                nextIntent.putExtra("ClassDescription", intentBundle.getString("ClassDescription"));
                                nextIntent.putExtra("ClassCode", intentBundle.getString("ClassCode"));
                                nextIntent.putExtra("Enrolled", true);
                                sentIntent = nextIntent;
                                intentBundle = sentIntent.getExtras();
                                isEnrolled = true;
                                Toast.makeText(getApplicationContext(), "Enrollment successful!", Toast.LENGTH_LONG).show();
                                EnrollmentButton.setText("Cancel enrollment to hide your profile from others");
                                return;

                            } catch (JSONException e) {
                                //Log.e(TAG, "error");
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "An error occurred while enrolling in class. Please try again.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Handle Errors
                            System.out.println(error.getMessage() + ": " + error.getStackTrace());
                            Toast.makeText(getApplicationContext(), "An error occurred while enrolling in class. Please try again.", Toast.LENGTH_LONG).show();
                        }
                    });

                    jsonQueue.add(postRequest);
                } else {
                    //Toast.makeText(getApplicationContext(), "Deleting enrollment has not been implemented yet.", Toast.LENGTH_LONG).show();

                    final JSONObject objectToPost = new JSONObject();
                    try {
                        objectToPost.put("classid", intentBundle.getString("ClassCode"));
                        objectToPost.put("Facebook_Token", intentBundle.getString("UserID"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String testURL;
                    //try {
                    testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/removeEnrollment";

                    JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, testURL,
                            objectToPost, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //Log.e(TAG, "jsonObject = " + response.toString());
                            /******************************************************
                             * try {
                             *   JSON Manipulation occurs here. You need to know
                             *   how the JSON you are parsing is ordered to be able
                             *   to pull the String/ values from it.
                             * } catch (JSONException e) {
                             *   e.printStackTrace();
                             * }
                             *****************************************************/


                            try {
                                System.out.println(response);
                                if (!response.getString("success").equals("true")) {
                                    //System.err.println("NO RESPONSE");
                                    Toast.makeText(getApplicationContext(), "An error occurred while cancelling enrollment in class. Please try again.", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                //JSONObject newEnrollment = response.getJSONObject("enrollment");
                                //String desc = "desc";
                                //String name = "name";
                                //String classCode = newClass.getString("Hashcode");


                                //System.out.println("name = " + name + " | Description = " + desc + " | classCode = " + classCode);
                                /*
                                sentIntent.setClass(getApplicationContext(), ClassCreationConfirmationActivity.class);
                                sentIntent.putExtra("ClassName", newClass.getString("Name"));
                                sentIntent.putExtra("ClassDescription", newClass.getString("Description"));
                                sentIntent.putExtra("ClassCode", newClass.getString("Hashcode"));
                                startActivity(sentIntent);
                                finish();
                                */
                                Intent nextIntent = new Intent(getApplicationContext(), StudentTindrActivity.class);
                                nextIntent.putExtra("UserID", intentBundle.getString("UserID"));
                                nextIntent.putExtra("TokenID", intentBundle.getString("TokenID"));
                                nextIntent.putExtra("UserName", intentBundle.getString("UserName"));
                                nextIntent.putExtra("UserDescription", intentBundle.getString("UserDescription"));
                                nextIntent.putExtra("ClassName", intentBundle.getString("ClassName"));
                                nextIntent.putExtra("ClassDescription", intentBundle.getString("ClassDescription"));
                                nextIntent.putExtra("ClassCode", intentBundle.getString("ClassCode"));
                                nextIntent.putExtra("Enrolled", false);
                                sentIntent = nextIntent;
                                intentBundle = sentIntent.getExtras();
                                isEnrolled = false;
                                Toast.makeText(getApplicationContext(), "Enrollment cancelled", Toast.LENGTH_LONG).show();
                                EnrollmentButton.setText("Enroll in this class for others to see you");
                                return;

                            } catch (JSONException e) {
                                //Log.e(TAG, "error");
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "An error occurred while cancelling enrollment in class. Please try again.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Handle Errors
                            System.out.println(error.getMessage() + ": " + error.getStackTrace());
                            Toast.makeText(getApplicationContext(), "An error occurred while cancelling enrollment in class. Please try again.", Toast.LENGTH_LONG).show();
                        }
                    });

                    jsonQueue.add(postRequest);
                }
            }
        });

        /*//
        GroupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SessionLandingPageActivity.this, "view group", Toast.LENGTH_SHORT).show();
                Intent groupIntent = new Intent(SessionLandingPageActivity.this, ViewGroupsActivity.class);
                startActivity(groupIntent);

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("ViewGroupsActivity", "Setting screen name: View Groups");
                mTracker.setScreenName("View Groups");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
        //*/

        StudentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SessionLandingPageActivity.this, "view student", Toast.LENGTH_SHORT).show();
                //Intent studentIntent = new Intent(SessionLandingPageActivity.this, ViewStudentsActivity.class);
                sentIntent.setClass(getApplicationContext(), StudentTindrActivity.class);
                startActivity(sentIntent);

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("SessionLandingActivity", "Setting screen name: View Students");
                mTracker.setScreenName("View Students");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
        //*/
    }
}
