package com.example.tina.groupfindr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.w3c.dom.Text;

public class EnterSessionActivity extends AppCompatActivity {
    private TextView EnterSessionText;
    private TextView CreateSessionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_session);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        EnterSessionText = (TextView) findViewById(R.id.EnterSession_EnterSessionText);
        CreateSessionText = (TextView) findViewById(R.id.EnterSession_CreateSessionText);

        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
        Analytics application = (Analytics) getApplication();
        final Tracker mTracker = application.getDefaultTracker();

        EnterSessionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EnterSessionActivity.this, "enter session", Toast.LENGTH_SHORT).show();
                Intent groupIntent = new Intent(EnterSessionActivity.this, JoinSessionActivity.class);
                startActivity(groupIntent);

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("EnterSessionActivity", "Setting screen name: Enter Session");
                mTracker.setScreenName("Enter Session");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });

        CreateSessionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EnterSessionActivity.this, "create session", Toast.LENGTH_SHORT).show();
                Intent studentIntent = new Intent(EnterSessionActivity.this, ClassCreationActivity.class);
                startActivity(studentIntent);

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("EnterSessionActivity", "Setting screen name: Create Session");
                mTracker.setScreenName("Create Session");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
    }
}
