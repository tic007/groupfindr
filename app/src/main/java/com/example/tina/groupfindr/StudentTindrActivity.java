package com.example.tina.groupfindr;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StudentTindrActivity extends AppCompatActivity {

    private TextView UserName;
    private TextView UserDescription;
    private Button FacebookButton;
    private Button PrevButton;
    private Button NextButton;

    private Intent sentIntent;
    private Bundle intentBundle;
    private RequestQueue jsonQueue;

    private int userIndex;
    private ArrayList<User_Model> userList;
    private String curFacebookURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_tindr);

        sentIntent = getIntent();
        intentBundle = sentIntent.getExtras();
        jsonQueue = VolleyQueue.getInstance(this.getApplicationContext()).ourInstance;

        UserName = (TextView) findViewById(R.id.Tindr_Name);
        UserDescription = (TextView) findViewById(R.id.Tindr_Description);
        FacebookButton = (Button) findViewById(R.id.Tindr_Facebook);
        PrevButton = (Button) findViewById(R.id.Tindr_Prev);
        NextButton = (Button) findViewById(R.id.Tindr_Next);
        userList = new ArrayList<User_Model>();
        //userList.add(new User_Model(intentBundle.getString("UserDescription"), intentBundle.getString("UserName"), intentBundle.getString("UserID")));
        userIndex = 0;
        curFacebookURL = "https://www.facebook.com/";

        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
        Analytics application = (Analytics) getApplication();
        final Tracker mTracker = application.getDefaultTracker();

        // Adding test information
        /*
        userList.add(new User_Model("I'm free on weekends.  I'm also super awesome, dependable, and easy to work with.", "Tina Chang", "10101"));
        userList.add(new User_Model("I'm free on Tuesdays and Fridays, and I specialize in server development and ASP.NET", "Shawn Palmer", "2"));
        userList.add(new User_Model("I'm free on afternoons every day except for Thursday.  You can reach me at 1-555-093-0205.  I'm great at presenting and speaking.", "John Marks", "3"));
        */

        //Handlers Here
        /*
        UserName.setText(intentBundle.getString("UserName"));
        UserDescription.setText(intentBundle.getString("UserDescription"));
        */

        // Add users using JSON Request

        final JSONObject requestObject = new JSONObject();
        /*
        try {
            requestObject.put("classid", intentBundle.getString("ClassCode"));
            requestObject.put("Facebook_Token", intentBundle.getString("UserID"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */
        String testURL;
        //try {
        testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/classmate/" + intentBundle.get("ClassCode");

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, testURL,
                requestObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.e(TAG, "jsonObject = " + response.toString());
                /******************************************************
                 * try {
                 *   JSON Manipulation occurs here. You need to know
                 *   how the JSON you are parsing is ordered to be able
                 *   to pull the String/ values from it.
                 * } catch (JSONException e) {
                 *   e.printStackTrace();
                 * }
                 *****************************************************/


                try {
                    System.out.println(response);
                    if (!response.getString("success").equals("true")) {
                        //System.err.println("NO RESPONSE");
                        Toast.makeText(getApplicationContext(), "An error occurred while getting users. Please try again.", Toast.LENGTH_LONG).show();
                        return;
                    }
                    JSONArray jsonUsers = response.getJSONArray("result");
                    //String desc = "desc";
                    //String name = "name";
                    //String classCode = newClass.getString("Hashcode");


                    //System.out.println("name = " + name + " | Description = " + desc + " | classCode = " + classCode);
                                /*
                                sentIntent.setClass(getApplicationContext(), ClassCreationConfirmationActivity.class);
                                sentIntent.putExtra("ClassName", newClass.getString("Name"));
                                sentIntent.putExtra("ClassDescription", newClass.getString("Description"));
                                sentIntent.putExtra("ClassCode", newClass.getString("Hashcode"));
                                startActivity(sentIntent);
                                finish();
                                */
                    /*
                    Intent nextIntent = new Intent(getApplicationContext(), StudentTindrActivity.class);
                    nextIntent.putExtra("UserID", intentBundle.getString("UserID"));
                    nextIntent.putExtra("TokenID", intentBundle.getString("TokenID"));
                    nextIntent.putExtra("UserName", intentBundle.getString("UserName"));
                    nextIntent.putExtra("UserDescription", intentBundle.getString("UserDescription"));
                    nextIntent.putExtra("ClassName", intentBundle.getString("ClassName"));
                    nextIntent.putExtra("ClassDescription", intentBundle.getString("ClassDescription"));
                    nextIntent.putExtra("ClassCode", intentBundle.getString("ClassCode"));
                    nextIntent.putExtra("Enrolled", true);
                    sentIntent = nextIntent;
                    intentBundle = sentIntent.getExtras();
                    isEnrolled = true;
                    Toast.makeText(getApplicationContext(), "Enrollment successful!", Toast.LENGTH_LONG).show();
                    EnrollmentButton.setText("Cancel enrollment to hide your profile from others");
                    */

                    for(int i = 0; i < jsonUsers.length(); i++)
                    {
                        JSONObject nextUser = jsonUsers.getJSONObject(i);
                        userList.add(new User_Model(nextUser.getString("Description"), nextUser.getString("Name"), nextUser.getString("Facebook_Token")));
                    }
                    return;

                } catch (JSONException e) {
                    //Log.e(TAG, "error");
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "An error occurred while getting users. Please try again.", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Handle Errors
                System.out.println(error.getMessage() + ": " + error.getStackTrace());
                Toast.makeText(getApplicationContext(), "An error occurred while getting users. Please try again.", Toast.LENGTH_LONG).show();
            }
        });

        jsonQueue.add(postRequest);

        if(userList.size() > 0) {
            setUser(0);
        }

        FacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userList.size() > 0) {
                    Uri uri = Uri.parse(curFacebookURL);

                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);

                    // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                    Log.i("StudentTindrActivity", "FB Profile");
                    mTracker.setScreenName("FB Profile");
                    mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                }
            }
        });

        PrevButton.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPrevUser();

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("StudentTindrActivity", "Setting screen name: Previous");
                mTracker.setScreenName("Previous");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });

        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNextUser();

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("StudentTindrActivity", "Setting screen name: Next");
                mTracker.setScreenName("Next");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
    }

    protected void setUser(int i) {
        userIndex = i;

        User_Model next = userList.get(i);
        UserName.setText(next.Name);
        UserDescription.setText(next.Description);

        curFacebookURL = "https://www.facebook.com/" + next.Facebook_Token;
    }

    protected void getNextUser() {
        if(userList.size() > 0) {
            //System.out.println("Getting user at index " + ((userIndex + 1) % (userList.size())));
            //System.out.println("Size of userList is " + userList.size());
            setUser((userIndex + 1) % (userList.size()));
            //userIndex = (userIndex + 1) % (userList.size());
        }
    }

    protected void getPrevUser(){
        if(userList.size() > 0) {
            if(userIndex <= 0)
            {
                userIndex = userList.size();
            }
            //System.out.println("Getting user at index " + ((userIndex -1)));
            //System.out.println("Size of userList is " + userList.size());
            setUser((userIndex-1));
            //userIndex = userIndex - 1;
        }
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_tindr, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
