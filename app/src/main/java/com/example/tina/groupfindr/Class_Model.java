package com.example.tina.groupfindr;

/**
 * Created by Tina on 11/21/15.
 */
public class Class_Model {
    public String Name;
    public String Hashcode;

    public Class_Model(String n, String h) {
        Name = n;
        Hashcode = h;
    }

    @Override
    public String toString()
    {
        return Name + ": " + Hashcode;
    }
}
