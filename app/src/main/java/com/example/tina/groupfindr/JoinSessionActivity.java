package com.example.tina.groupfindr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.*;
import com.facebook.*;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JoinSessionActivity extends AppCompatActivity {
    private EditText mClassCode;
    private Button mJoinSession;
    private Button mCreateSession;
    private Button mUpdateInfo;
    //private Button button;
    private ListView mClassList;
    private Intent sentIntent;
    private Bundle intentBundle;
    private RequestQueue jsonQueue;

    private Analytics application;
    private Tracker mTracker;

    private ArrayList<Class_Model> userClasses;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            android.os.Process.killProcess(android.os.Process.myPid());
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    private void joinSession(String classCode)
    {
        JSONObject request = new JSONObject();
        try {
            request.put("class_code", classCode);//mClassCode.getText().toString());
            request.put("user_token", intentBundle.getString("UserID"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String testURL;
        testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/getClass";

        System.out.println("JoinSession Request: " + request);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, testURL, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e(TAG, "jsonObject = " + response.toString());
                        /******************************************************
                         * try {
                         *   JSON Manipulation occurs here. You need to know
                         *   how the JSON you are parsing is ordered to be able
                         *   to pull the String/ values from it.
                         * } catch (JSONException e) {
                         *   e.printStackTrace();
                         * }
                         *****************************************************/
                        System.out.println("getClass Response: " + response);
                        try {
                            if (!response.getBoolean("success")) {
                                System.err.println("Error encountered");
                                Toast.makeText(getApplicationContext(), "Could not find your class. Please check your code and try again.", Toast.LENGTH_LONG).show();
                                return;
                            } else {
                                Boolean enrolled = response.getBoolean("enrolled");
                                JSONObject foundClass = response.getJSONObject("result");
                                if (enrolled) {
                                    JSONObject enrollment = response.getJSONObject("enrollment");
                                    System.out.println("Enrollment JSON: " + enrollment);

                                    sentIntent.setClass(getApplicationContext(), SessionLandingPageActivity.class);
                                    sentIntent.putExtra("ClassName", foundClass.getString("Name"));
                                    sentIntent.putExtra("ClassDescription", foundClass.getString("Description"));
                                    sentIntent.putExtra("ClassCode", foundClass.getString("Hashcode"));
                                    sentIntent.putExtra("Enrolled", enrolled);
                                    startActivity(sentIntent);
                                    //finish();

                                    // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                                    Log.i("JoinSessionActivity", "Setting screen name: Join Session Enrolled");
                                    mTracker.setScreenName("Join Session Enrolled");
                                    mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                                    return;
                                } else {
                                    sentIntent.setClass(getApplicationContext(), SessionLandingPageActivity.class);
                                    sentIntent.putExtra("ClassName", foundClass.getString("Name"));
                                    sentIntent.putExtra("ClassDescription", foundClass.getString("Description"));
                                    sentIntent.putExtra("ClassCode", foundClass.getString("Hashcode"));
                                    sentIntent.putExtra("Enrolled", enrolled);
                                    startActivity(sentIntent);
                                    //finish();

                                    // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                                    Log.i("JoinSessionActivity", "Setting screen name: Join Session Not Enrolled");
                                    mTracker.setScreenName("Join Session Not Enrolled");
                                    mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                                    return;
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Handle Errors
                System.out.println(error.getMessage() + ": " + error.getStackTrace());
                Toast.makeText(getApplicationContext(), "Could not find your class. Please check your code and try again.", Toast.LENGTH_LONG).show();
            }
        });

        jsonQueue.add(getRequest);
    }

    private class StableArrayAdapter extends ArrayAdapter<Class_Model>
    {
        HashMap<Class_Model, Integer> mIdMap = new HashMap<Class_Model, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId, List<Class_Model> classes)
        {
            super(context, textViewResourceId, classes);
            for(int i = 0; i < classes.size(); i++)
            {
                mIdMap.put(classes.get(i), i);
            }
        }
    }

    private void getSessions()
    {
        JSONObject request = new JSONObject();
        /*
        try {
            request.put("class_code", classCode);//mClassCode.getText().toString());
            request.put("user_token", intentBundle.getString("UserID"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */

        String testURL;
        testURL = "http://ec2-54-193-13-38.us-west-1.compute.amazonaws.com:8080/api/Enrollment/" + intentBundle.getString("UserID");

        //System.out.println("JoinSession Request: " + request);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, testURL, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e(TAG, "jsonObject = " + response.toString());
                        /******************************************************
                         * try {
                         *   JSON Manipulation occurs here. You need to know
                         *   how the JSON you are parsing is ordered to be able
                         *   to pull the String/ values from it.
                         * } catch (JSONException e) {
                         *   e.printStackTrace();
                         * }
                         *****************************************************/
                        System.out.println("getClass Response: " + response);
                        try {
                            if (!response.getBoolean("success")) {
                                System.err.println("Error encountered");
                                Toast.makeText(getApplicationContext(), "Error occurred while loading enrolled sessions.", Toast.LENGTH_LONG).show();
                                return;
                            } else {
                                //Boolean enrolled = response.getBoolean("enrolled");
                                //JSONObject enrolledClass = response.getJSONObject("enrolled");
                                JSONArray enrolledClass = response.getJSONArray("enrolled");
                                JSONArray ownedClass = response.getJSONArray("owned");
                                //JSONArray result = response.getJSONArray("result");
                                System.out.println("Enrolled JSON: " + enrolledClass);
                                System.out.println("Owned JSON: " + ownedClass);

                                for(int i = 0; i < ownedClass.length(); i++)
                                {
                                    JSONObject next = ownedClass.getJSONObject(i);
                                    Class_Model nextClass = new Class_Model(next.getString("Name"), next.getString("Hashcode"));
                                    if(!nextClass.Name.equals(""))
                                        userClasses.add(nextClass);
                                }

                                for(int i = 0; i < enrolledClass.length(); i++)
                                {
                                    JSONObject next = enrolledClass.getJSONObject(i);
                                    Class_Model nextClass = new Class_Model(next.getString("Name"), next.getString("Hashcode"));
                                    if(!nextClass.Name.equals(""))
                                        userClasses.add(nextClass);
                                }

                                final StableArrayAdapter adapter = new StableArrayAdapter(getApplicationContext(), R.layout.custom_textview, userClasses);

                                mClassList.setAdapter(adapter);

                                mClassList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                    @Override
                                    public void onItemClick(AdapterView<?> parent, final View view,
                                                            int position, long id) {
                                        final Class_Model item = (Class_Model) parent.getItemAtPosition(position);
                                        joinSession(item.Hashcode);
                                    }

                                });

                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error occurred while loading enrolled sessions.", Toast.LENGTH_LONG).show();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Handle Errors
                System.out.println(error.getMessage() + ": " + error.getStackTrace());
                Toast.makeText(getApplicationContext(), "Error occurred while loading enrolled sessions.", Toast.LENGTH_LONG).show();
            }
        });

        jsonQueue.add(getRequest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_session);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //button = (Button) findViewById(R.id.enterSession_loginButton);
        mClassCode = (EditText) findViewById(R.id.enterSession_classCode);
        mJoinSession = (Button) findViewById(R.id.enterSession_joinSession);
        mCreateSession = (Button) findViewById(R.id.enterSession_createSession);
        mUpdateInfo = (Button) findViewById(R.id.enterSession_updateInfo);
        mClassList = (ListView) findViewById(R.id.enterSession_classList);
        jsonQueue = VolleyQueue.getInstance(this.getApplicationContext()).ourInstance;
        sentIntent = getIntent();
        intentBundle = sentIntent.getExtras();

        userClasses = new ArrayList<Class_Model>();
        getSessions();

        // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
        application = (Analytics) getApplication();
        mTracker = application.getDefaultTracker();

        System.out.println(intentBundle.getString("UserName"));
        System.out.println(intentBundle.getString("UserDescription"));


        mJoinSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClassCode.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter a class code to continue.", Toast.LENGTH_LONG).show();
                    return;
                }

                joinSession(mClassCode.getText().toString());
            }
        });

        mCreateSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sentIntent.setClass(getApplicationContext(), ClassCreationActivity.class);
                startActivity(sentIntent);
                finish();

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("JoinSessionActivity", "Setting screen name: Create Session");
                mTracker.setScreenName("Create Session");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                return;
            }
        });

        mUpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(getApplicationContext(), UserInfoActivity.class);
                newIntent.putExtra("UserID", intentBundle.getString("UserID"));
                newIntent.putExtra("TokenID", intentBundle.getString("TokenID"));
                newIntent.putExtra("UserName", intentBundle.getString("UserName"));
                newIntent.putExtra("UserDescription", intentBundle.getString("UserDescription"));
                startActivity(newIntent);
                finish();

                // Code taken from https://developers.google.com/analytics/devguides/collection/android/v4/
                Log.i("JoinSessionActivity", "Setting screen name: Set User Info");
                mTracker.setScreenName("Set User Info");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                return;
            }
        });

    }

}
